SPAM = 1
trainPositive = {}
trainNegative = {}
positiveTotal = 0
negativeTotal = 0
pA = 0
pNotA = 0
#runs once on training data
def train(trainData):
    total = 0
    numSpam = 0
    for email in trainData:
        if email.label == SPAM:
            numSpam += 1
        total += 1
        processEmail(email.body, email.label, positiveTotal,negativeTotal)

    pA = float(numSpam)/float(total)
    pNotA = float(total) - float(numSpam)/float(total)


#counts the words in a specific email
def processEmail(body, label, positiveTotal, negativeTotal):
    for word in body:
        if label == SPAM:
            trainPositive[word] = trainPositive.get(word, 0) + 1
            positiveTotal += 1
        else:
            trainNegative[word] = trainNegative.get(word, 0) + 1
            negativeTotal += 1

#gives the conditional probability p(B_i | A_x)
def conditionalWord(word, spam):
    if spam:
       return trainPositive[word]/float(positiveTotal)
    return trainNegative[word]/float(negativeTotal)




#gives the conditional probability p(B | A_x)
def conditionalEmail(body, spam):
    result = 1.0
    for word in body:
        result *= conditionalWord(word, spam)
    return result


# #gives the conditional probability p(B_i | A_x) with smoothing
# def conditionalWord(word, spam):
#     if spam:
#        return (trainPositive.get(word,0)+alpha)/(float)(positiveTotal+alpha*numWords)
#     return (trainNegative.get(word,0)+alpha)/(float)(negativeTotal+alpha*numWords)



#classifies a new email as spam or not spam
def classify(email):
    isSpam = pA * conditionalEmail(email, True) # P (A | B)
    notSpam = pNotA * conditionalEmail(email, False) # P(¬A | B)
    return isSpam > notSpam